package com.amd.carmain.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.amd.carmain.R;

import java.util.Objects;

import biz.kasual.materialnumberpicker.MaterialNumberPicker;

import static com.amd.carmain.view.SettingsFragment.SETTINGS_PREFERENCE_FILE_KEY;

public class NumberPickerDialog extends DialogFragment {

    private static final String PREFERENCE_KEY_ARGS = "preferencesKey";
    private static final String TITLE_KEY_ARGS = "title";
    private static final String MIN_KEY_ARGS = "min";
    private static final String MAX_KEY_ARGS = "max";
    private static final String DEFAULT_VAL_KEY_ARGS = "defaultVal";

    private String SETTINGS_PREFERENCE_KEY;

    private TextView tvTitle;
    private MaterialNumberPicker numberPicker;
    private Button btnChange;
    private Button btnCancel;

    public NumberPickerDialog() {
    }

    //create new instance and add detail to bundle
    public static NumberPickerDialog newInstance(String preferencesKey, String title, int min, int max, int defaultVal) {
        NumberPickerDialog dialog = new NumberPickerDialog();
        Bundle args = new Bundle();

        args.putString(PREFERENCE_KEY_ARGS, preferencesKey);
        args.putString(TITLE_KEY_ARGS, title);
        args.putInt(MIN_KEY_ARGS, min);
        args.putInt(MAX_KEY_ARGS, max);
        args.putInt(DEFAULT_VAL_KEY_ARGS, defaultVal);
        dialog.setArguments(args);

        return dialog;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_number_picker, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDialog().setCancelable(false);

        assert getArguments() != null;
        SETTINGS_PREFERENCE_KEY = getArguments().getString(PREFERENCE_KEY_ARGS);

        String title = getArguments().getString(TITLE_KEY_ARGS, getString(R.string.enter_new_number));
        getDialog().setTitle(title);
        tvTitle = view.findViewById(R.id.textView_numberPicker_title);
        tvTitle.setText(title);

        int min = getArguments().getInt(MIN_KEY_ARGS, 1);
        int max = getArguments().getInt(MAX_KEY_ARGS, 100);
        int defaultVal = getArguments().getInt(DEFAULT_VAL_KEY_ARGS, 50);
        numberPicker = view.findViewById(R.id.numberPicker_picker);
        numberPicker.setMinValue(min);
        numberPicker.setMaxValue(max);
        numberPicker.setValue(defaultVal);
        numberPicker.setWrapSelectorWheel(true);

        btnChange = view.findViewById(R.id.button_numberPicker_change);
        btnCancel = view.findViewById(R.id.button_numberPicker_cancel);

        btnChange.setOnClickListener(v -> changeListener());
        btnCancel.setOnClickListener(v -> cancelListener());
    }

    private void changeListener() {

        btnChange.setClickable(false);

        int value = numberPicker.getValue();
        SharedPreferences sharedPref = getActivity().getSharedPreferences(
                SETTINGS_PREFERENCE_FILE_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(SETTINGS_PREFERENCE_KEY, value);
        editor.apply();

        Objects.requireNonNull(NumberPickerDialog.this.getDialog()).cancel();
    }

    private void cancelListener() {
        Objects.requireNonNull(NumberPickerDialog.this.getDialog()).cancel();
    }

}
