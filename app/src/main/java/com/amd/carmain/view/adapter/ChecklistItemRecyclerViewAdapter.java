package com.amd.carmain.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amd.carmain.R;
import com.amd.carmain.data.ChecklistDB;
import com.amd.carmain.data.ExpireDB;
import com.amd.carmain.data.model.Checklist;
import com.amd.carmain.data.model.Expire;
import com.amd.carmain.view.ChecklistDetailActivity;
import com.google.android.material.snackbar.Snackbar;

import java.util.Collections;
import java.util.List;

import static com.amd.carmain.data.DateFormatterUtil.dateFormatter;
import static com.amd.carmain.view.ChecklistDetailActivity.CHECKLIST_ID;


public class ChecklistItemRecyclerViewAdapter extends RecyclerView.Adapter<ChecklistItemRecyclerViewAdapter.MyViewHolder> {

    private final List<Checklist> checklists;
    private Checklist recentlyDeletedChecklist;
    private int recentlyDeletedPosition;
    public enum Sort {
        TITLE,
        TITLE_DESC,
        DATE,
        DATE_DESC,
        EXPIRED,
        EXPIRED_DESC
    }

    public ChecklistItemRecyclerViewAdapter(List<Checklist> checklists) {
        this.checklists = checklists;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View contactView = inflater.inflate(R.layout.checklist_item, parent, false);
        return new MyViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Checklist item = checklists.get(position);
        ImageView ivLogo = holder.getIvLogo();
        TextView tvTitle = holder.getTvTitle();
        TextView tvExpire = holder.getTvExpire();
        ImageButton ibtnNotification = holder.getIbtnNotification();
        TextView tvNote = holder.getTvNote();
        TextView tvChangeDate = holder.getTvChangeDate();
        TextView tvChangeKilometer = holder.getTvChangeKilometer();

        tvTitle.setText(item.getTitle());

        if (!item.getExpire().isExpired()) {
            ivLogo.setImageResource(R.drawable.ic_car_service_good);
            ibtnNotification.setVisibility(View.VISIBLE);
            if (item.getExpire().isNotificationOn())
                ibtnNotification.setImageResource(R.drawable.ic_notifications_active);
            else
                ibtnNotification.setImageResource(R.drawable.ic_notifications_off);
        } else {
            ivLogo.setImageResource(R.drawable.ic_car_service_bad);
            ibtnNotification.setVisibility(View.GONE);
        }

        if (item.getExpire().expireMode() == Expire.EXPIRE_DATE)
            tvExpire.setText(dateFormatter(item.getExpire().getExpireDate()));
        else
            tvExpire.setText(item.getExpire().getExpireKilometer() + " Km");

        if (TextUtils.isEmpty(item.getNote()))
            tvNote.setText(R.string.n_a);
        else
            tvNote.setText(item.getNote());

        tvChangeDate.setText(dateFormatter(item.getChangeDate()));
        tvChangeKilometer.setText(String.valueOf(item.getChangeKilometer()));
    }

    @Override
    public int getItemCount() {
        return checklists.size();
    }

    public void removeItem(int position) {
        checklists.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(Checklist item, int position) {
        checklists.add(position, item);
        notifyItemInserted(position);
    }

    public void sort(Sort sort) {
        switch (sort) {
            case TITLE:
                Collections.sort(checklists, (c1, c2) -> {
                    if (c1.getTitle() == null || c2.getTitle() == null)
                        return 0;
                    return c1.getTitle().toUpperCase().compareTo(c2.getTitle().toUpperCase());
                });
                break;
            case TITLE_DESC:
                Collections.sort(checklists, (c1, c2) -> {
                    if (c2.getTitle() == null || c1.getTitle() == null)
                        return 0;

                    return c2.getTitle().toUpperCase().compareTo(c1.getTitle().toUpperCase());
                });
                break;
            case DATE:
                Collections.sort(checklists, (c1, c2) -> {
                    if (c1.getChangeDate() == null || c2.getChangeDate() == null)
                        return 0;
                    return c1.getChangeDate().compareTo(c2.getChangeDate());
                });
                break;
            case DATE_DESC:
                Collections.sort(checklists, (c1, c2) -> {
                    if (c2.getChangeDate() == null || c1.getChangeDate() == null)
                        return 0;
                    return c2.getChangeDate().compareTo(c1.getChangeDate());
                });
                break;
            case EXPIRED:
                Collections.sort(checklists, (c1, c2) -> {
                    boolean b1 = c1.getExpire().isExpired();
                    boolean b2 = c2.getExpire().isExpired();
                    return (b1 != b2) ? (b1) ? -1 : 1 : 0;
                });
                break;
            default:
            case EXPIRED_DESC:
                Collections.sort(checklists, (c1, c2) -> {
                    boolean b1 = c2.getExpire().isExpired();
                    boolean b2 = c1.getExpire().isExpired();
                    return (b1 != b2) ? (b1) ? -1 : 1 : 0;
                });
        }
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final Context context;
        private final ImageView ivLogo;
        private final TextView tvTitle;
        private final TextView tvExpire;
        private final TextView tvNote;
        private final TextView tvChangeDate;
        private final TextView tvChangeKilometer;
        private final Button btnRenew;
        private final Button btnRemove;
        private final ImageButton ibtnNotification;
        private final ImageView ivExpand;
        private final RelativeLayout rlExpandable;

        public MyViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();
            ivLogo = itemView.findViewById(R.id.imageView_checklistItem_logo);
            tvTitle = itemView.findViewById(R.id.textView_checklistItem_title);
            tvExpire = itemView.findViewById(R.id.textView_checklistItem_expireValue);
            tvNote = itemView.findViewById(R.id.textView_checklistItem_noteValue);
            tvChangeDate = itemView.findViewById(R.id.textView_checklistItem_changeDateValue);
            tvChangeKilometer = itemView.findViewById(R.id.textView_checklistItem_changeKilometerValue);
            btnRenew = itemView.findViewById(R.id.button_checklistItem_renew);
            btnRemove = itemView.findViewById(R.id.button_checklistItem_remove);
            ibtnNotification = itemView.findViewById(R.id.imageButton_checklistItem_notification);
            ivExpand = itemView.findViewById(R.id.imageView_checklistItem_expand);
            rlExpandable = itemView.findViewById(R.id.relativeLayout_checklistItem_expandable);

            btnRenew.setOnClickListener(view -> {
                int position = getAdapterPosition();
                // Check if an item was deleted but the user clicks on it before disappeared
                if (position != RecyclerView.NO_POSITION) {
                    int id = checklists.get(position).getId();

                    Intent intent = new Intent(view.getContext(), ChecklistDetailActivity.class);
                    intent.putExtra(CHECKLIST_ID, id);
                    view.getContext().startActivity(intent);
                }
            });

            btnRemove.setOnClickListener(view -> {
                recentlyDeletedPosition = getAdapterPosition(); // save position of removed item for undoing
                if (recentlyDeletedPosition != RecyclerView.NO_POSITION) { // Check if an item was deleted but the user clicks on it before disappeared
                    recentlyDeletedChecklist = checklists.get(recentlyDeletedPosition); // save removed item for undoing
                    ChecklistDB db = new ChecklistDB(context);
                    db.delete(recentlyDeletedChecklist); // delete from db
                    removeItem(recentlyDeletedPosition); // delete from list

                    Snackbar.make(view, R.string.item_removed, Snackbar.LENGTH_LONG)  // Make snackbar for undo removing
                            .setAction(R.string.undo, view1 -> {
                                db.create(recentlyDeletedChecklist);
                                restoreItem(recentlyDeletedChecklist, recentlyDeletedPosition);
                            })
                            .show();
                }
            });

            ibtnNotification.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) { // Check if an item was deleted but the user clicks on it before disappeared
                    int id = checklists.get(position).getId();
                    ChecklistDB checklistDB = new ChecklistDB(context);
                    Checklist item = checklistDB.findById(id);

                    if (!item.getExpire().isExpired()) {
                        ExpireDB expireDB = new ExpireDB(context);
                        if (item.getExpire().isNotificationOn()) {
                            ibtnNotification.setImageResource(R.drawable.ic_notifications_off);
                            item.getExpire().setNotificationOn(false);
                        } else {
                            ibtnNotification.setImageResource(R.drawable.ic_notifications_active);
                            item.getExpire().setNotificationOn(true);
                        }
                        expireDB.update(item.getExpire());
                    }
                }
            });

            ivExpand.setOnClickListener(v ->{
                if (rlExpandable.getVisibility() == View.GONE) {
                    ivExpand.setImageResource(R.drawable.ic_arrow_up);
                    rlExpandable.setVisibility(View.VISIBLE);
                } else {
                    ivExpand.setImageResource(R.drawable.ic_arrow_down);
                    rlExpandable.setVisibility(View.GONE);
                }
            });
        }

        public Context getContext() {
            return context;
        }

        public ImageView getIvLogo() {
            return ivLogo;
        }

        public TextView getTvTitle() {
            return tvTitle;
        }

        public ImageButton getIbtnNotification() {
            return ibtnNotification;
        }

        public TextView getTvExpire() {
            return tvExpire;
        }

        public TextView getTvNote() {
            return tvNote;
        }

        public TextView getTvChangeDate() {
            return tvChangeDate;
        }

        public TextView getTvChangeKilometer() {
            return tvChangeKilometer;
        }
    }
}
