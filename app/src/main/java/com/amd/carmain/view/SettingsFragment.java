package com.amd.carmain.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;

import com.amd.carmain.R;

public class SettingsFragment extends Fragment {

    public static final String SETTINGS_PREFERENCE_FILE_KEY = "SETTINGS_PREFERENCE_FILE_KEY";
    public static final String SETTINGS_PREFERENCE_SPEED_METER_MAX = "SETTINGS_PREFERENCE_SPEED_METER_MAX";

    private LinearLayoutCompat llWifiCredential;
    private LinearLayoutCompat llFakeKilometer;
    private LinearLayoutCompat llSpeedMeterMax;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {

        llWifiCredential = view.findViewById(R.id.linearLayout_wifiCredential);
        llFakeKilometer = view.findViewById(R.id.linearLayout_fakeKilometer);
        llSpeedMeterMax = view.findViewById(R.id.linearLayout_speedMeter);

        llWifiCredential.setOnClickListener(v -> wifiCredential());
        llFakeKilometer.setOnClickListener(v -> fakeKilometer());
        llSpeedMeterMax.setOnClickListener(v -> speedMeterMax());

    }

    private void wifiCredential() {
        WifiCredentialDialog wifiCredentialDialog = new WifiCredentialDialog();

        wifiCredentialDialog.show(getParentFragmentManager(), "WifiCredentialDialog");
    }

    private void fakeKilometer() {
        ChangeKilometerDialog changeKilometerDialog = new ChangeKilometerDialog();

        changeKilometerDialog.show(getParentFragmentManager(), "ChangeKilometerDialog");
    }

    private void speedMeterMax() {

        SharedPreferences sharedPref = getActivity().getSharedPreferences(SETTINGS_PREFERENCE_FILE_KEY, Context.MODE_PRIVATE);
        int currentMaxSpeed = sharedPref.getInt(SETTINGS_PREFERENCE_SPEED_METER_MAX, 200);

        NumberPickerDialog speedMeterMax =
                NumberPickerDialog.newInstance(
                        SETTINGS_PREFERENCE_SPEED_METER_MAX,
                        getString(R.string.Enter_max_speed),
                        120,
                        300,
                        currentMaxSpeed);

        speedMeterMax.show(getParentFragmentManager(), "speedmeter_max_dialog");
    }

}
