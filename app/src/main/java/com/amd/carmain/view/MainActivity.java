package com.amd.carmain.view;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.amd.carmain.R;
import com.amd.carmain.service.ChecklistDateJobService;
import com.amd.carmain.service.ChecklistKilometerJobService;
import com.google.android.material.bottomnavigation.BottomNavigationView;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_dashboard, R.id.navigation_checklist)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);
    }

    @Override
    protected void onStart() {
        super.onStart();

        startChecklistJobScheduler(this);
    }

    public void startChecklistJobScheduler(Context context) {

        ComponentName checklistDateComponent = new ComponentName(context, ChecklistDateJobService.class);
        ComponentName checklistKilometerComponent = new ComponentName(context, ChecklistKilometerJobService.class);

        JobInfo.Builder checklistDateJobInfo;
        JobInfo.Builder checklistMileageJobInfo;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            checklistDateJobInfo = new JobInfo.Builder(0, checklistDateComponent)
                    .setMinimumLatency(900000)
                    .setPersisted(true);
            checklistMileageJobInfo = new JobInfo.Builder(1, checklistKilometerComponent)
                    .setMinimumLatency(900000)
                    .setPersisted(true)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED);
        } else {
            checklistDateJobInfo = new JobInfo.Builder(0, checklistDateComponent)
                    .setPeriodic(10000)
                    .setPersisted(true);
            checklistMileageJobInfo = new JobInfo.Builder(1, checklistKilometerComponent)
                    .setPeriodic(10000)
                    .setPersisted(true)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED);
        }

        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(checklistDateJobInfo.build());
        jobScheduler.schedule(checklistMileageJobInfo.build());

    }
}