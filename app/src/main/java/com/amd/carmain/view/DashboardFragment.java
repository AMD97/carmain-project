package com.amd.carmain.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.amd.carmain.R;
import com.amd.carmain.api.ModuleAPI;
import com.amd.carmain.api.ModuleAPIUtil;
import com.amd.carmain.data.model.CarDiagnostic;
import com.amd.carmain.service.NSDLookupHelper;
import com.github.anastr.speedviewlib.PointerSpeedometer;
import com.google.android.material.snackbar.Snackbar;
import com.iammert.tileprogressview.TiledProgressView;
import com.ramijemli.percentagechartview.PercentageChartView;
import com.ramijemli.percentagechartview.callback.AdaptiveColorProvider;
import com.ramijemli.percentagechartview.callback.ProgressTextFormatter;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import pub.devrel.easypermissions.EasyPermissions;

import static com.amd.carmain.view.SettingsFragment.SETTINGS_PREFERENCE_FILE_KEY;
import static com.amd.carmain.view.SettingsFragment.SETTINGS_PREFERENCE_SPEED_METER_MAX;

public class DashboardFragment extends Fragment implements LocationListener {

    private final String TAG = "DashboardFragment";
    private final int Request_Code_LOCATION = 100;
    private final String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    private TextView tvConnectionStatus;
    private enum ConnectionStatus {CONNECTING, CONNECTED, DISCONNECTED};
    private PointerSpeedometer speedometer;
    private TiledProgressView prgsFuel;
    private PercentageChartView chrtPressureFR;
    private PercentageChartView chrtPressureFL;
    private PercentageChartView chrtPressureBR;
    private PercentageChartView chrtPressureBL;
    private TextView tvOilStatus;
    private TextView tvWaterStatus;
    private TextView tvBrakepadStatus;
    private ImageView ivOilIndicator;
    private ImageView ivWaterIndicator;
    private ImageView ivBrakepadIndicator;
    private SwipeRefreshLayout swipeRefreshLayout;
    private NSDLookupHelper nsdLookup;
    private Disposable disposable;
    private ModuleAPI moduleAPI;
    private LocationManager locationManager;
    private int connectionFailedCounter;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
        apiBuild();
        swipeRefListener();
        locationConfig();

    }

    private void initViews(View view) {
        tvConnectionStatus = view.findViewById(R.id.textView_dashboard_connection);

        speedometer = view.findViewById(R.id.speedmeter_dashboard);
        SharedPreferences sharedPref = getActivity().getSharedPreferences(SETTINGS_PREFERENCE_FILE_KEY, Context.MODE_PRIVATE);
        int maxSpeed = sharedPref.getInt(SETTINGS_PREFERENCE_SPEED_METER_MAX, 200);
        speedometer.setMaxSpeed(maxSpeed);

        prgsFuel = view.findViewById(R.id.progress_dashboard_fuel);
        prgsFuel.setColorRes(R.color.brown_100);
        prgsFuel.setLoadingColorRes(R.color.brown_600);
        chrtPressureFR = view.findViewById(R.id.chart_dashboard_pressureFR);
        chrtPressureFL = view.findViewById(R.id.chart_dashboard_pressureFL);
        chrtPressureBR = view.findViewById(R.id.chart_dashboard_pressureBR);
        chrtPressureBL = view.findViewById(R.id.chart_dashboard_pressureBL);
        tvOilStatus = view.findViewById(R.id.textView_dashboard_oilStatus);
        tvWaterStatus = view.findViewById(R.id.textView_dashboard_waterStatus);
        tvBrakepadStatus = view.findViewById(R.id.textView_dashboard_brakepadStatus);
        ivOilIndicator = view.findViewById(R.id.imageView_dashboard_oliIndicator);
        ivWaterIndicator = view.findViewById(R.id.imageView_dashboard_waterIndicator);
        ivBrakepadIndicator = view.findViewById(R.id.imageView_dashboard_brakepadIndicator);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefresh_dashboard);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Pressure text format for pressure chart
        ProgressTextFormatter progressTextFormatter =
                progress -> Math.round(progress) + " PSI";

        // Adaptive color provider for pressure chart
        AdaptiveColorProvider colorProvider =
                new PressureChartAdaptiveColorProvider(
                        getColor(R.color.green_600),
                        getColor(R.color.yellow_600),
                        getColor(R.color.red_900),
                        getColor(R.color.indigo_500),
                        getColor(R.color.indigo_700),
                        getColor(R.color.white));

        // assign AdaptiveColorProvider and text formatter
        chrtPressureFR.setAdaptiveColorProvider(colorProvider);
        chrtPressureFL.setAdaptiveColorProvider(colorProvider);
        chrtPressureBL.setAdaptiveColorProvider(colorProvider);
        chrtPressureBR.setAdaptiveColorProvider(colorProvider);
        chrtPressureFR.setTextFormatter(progressTextFormatter);
        chrtPressureFL.setTextFormatter(progressTextFormatter);
        chrtPressureBL.setTextFormatter(progressTextFormatter);
        chrtPressureBR.setTextFormatter(progressTextFormatter);

        startConnection();
    }

    // Swipe refresh listener for reconnecting to ESP8266 module
    private void swipeRefListener() {
        swipeRefreshLayout.setOnRefreshListener(() -> {
            startConnection();
            final Handler handler = new Handler();
            handler.postDelayed(() -> {
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }, 1000);
        });
    }

    @SuppressLint("MissingPermission")
    private void locationConfig() {
        // check location permissions and start location manager for measure speed
        try {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            if (EasyPermissions.hasPermissions(requireContext(), perms)) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        1000, 5, this);
            } else {
                EasyPermissions.requestPermissions(this,
                        "CarMain requires location access, manage in next step!",
                        Request_Code_LOCATION, perms);
            }
        } catch (IllegalStateException e) {
            Log.e(TAG, "locationConfig: " + e.toString());
        }
    }

    @Override
    public void onLocationChanged(Location newLoc) {
        speedometer.speedTo((float) (newLoc.getSpeed() * 3.6));
    }

    private void apiBuild() {
        nsdLookup = NSDLookupHelper.getInstance(getContext());
        if (!nsdLookup.isServiceOn())  // start nsdLookup service if service is off
            nsdLookup.registerService();

        moduleAPI = ModuleAPIUtil.newInstance(nsdLookup);
    }

    public void startConnection() {
        Log.d(TAG, "startConnection");

        tvConnectionStatus(DashboardFragment.ConnectionStatus.CONNECTING);

        connectionFailedCounter = 0;

        if (!nsdLookup.isServiceOn()) {  // start nsdLookup service if service is off
            Log.d(TAG, "nsdLookup start");
            nsdLookup.registerService();
        }
        if (disposable == null || disposable.isDisposed()) {
            disposable = Observable
                    .interval(1000, 1000, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::callModule,
                            e -> Log.e(TAG, "observableError: " + e.toString()));
        }
    }

    @SuppressLint("CheckResult")
    private void callModule(Long aLong) {
        Log.d(TAG, "callModule");

        Observable<CarDiagnostic> observable = moduleAPI.getDiagnostic(nsdLookup.getModuleURI());
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(result -> result)
                .subscribe(this::handleSuccess, this::handleError);
    }

    private void handleSuccess(CarDiagnostic data) {
        Log.d(TAG, "handleSuccess");

        tvConnectionStatus(DashboardFragment.ConnectionStatus.CONNECTED);

        prgsFuel.setProgress(data.getFuel());

        ivOilIndicator.setColorFilter(data.getOil() ? getColor(R.color.green_600) : getColor(R.color.red_800));
        tvOilStatus.setText(data.getOil() ? "Good" : "Bad");

        ivWaterIndicator.setColorFilter(data.getWater() ? getColor(R.color.green_600) : getColor(R.color.red_800));
        tvWaterStatus.setText(data.getWater() ? "Good" : "Bad");

        ivBrakepadIndicator.setColorFilter(data.getBrakePad() ? getColor(R.color.green_600) : getColor(R.color.red_800));
        tvBrakepadStatus.setText(data.getBrakePad() ? "Good" : "Bad");

        chrtPressureFR.setProgress(data.getTirePressureFR(), true);
        chrtPressureFL.setProgress(data.getTirePressureFL(), true);
        chrtPressureBR.setProgress(data.getTirePressureBR(), true);
        chrtPressureBL.setProgress(data.getTirePressureBL(), true);
    }

    private void handleError(Throwable t) {
        Log.d(TAG, "handleError" + t.getMessage());

        connectionFailedCounter++;

        if (connectionFailedCounter == 3) {
            if (!disposable.isDisposed())
                disposable.dispose();
            try {
                tvConnectionStatus(DashboardFragment.ConnectionStatus.DISCONNECTED);
                Snackbar.make(getView().findViewById(R.id.coordinatorLayout_dashboard_container),
                        "Please make sure wifi module connected",
                        Snackbar.LENGTH_LONG)
                        .setAction("Wifi setting", view1 ->
                                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)))
                        .show();
            } catch (Throwable e) {
                Log.e(TAG, "handleError: " + t.toString());
            }
        }
    }

    private void tvConnectionStatus(ConnectionStatus mode){
        switch (mode) {
            case CONNECTED:
                tvConnectionStatus.setText(R.string.Connected);
                tvConnectionStatus.setTextColor(getColor(R.color.white));
                break;
            case CONNECTING:
                tvConnectionStatus.setText(R.string.connecting);
                tvConnectionStatus.setTextColor(getColor(R.color.yellow_300));
                break;
            case DISCONNECTED:
                tvConnectionStatus.setText(R.string.Disconnected);
                tvConnectionStatus.setTextColor(getColor(R.color.red_600));
        }
    }

    private int getColor(int color) {
        return getResources().getColor(color);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {}

    @Override
    public void onProviderDisabled(@NonNull String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onPause() {
        super.onPause();

        if (disposable != null && !disposable.isDisposed())
            disposable.dispose();

        if (locationManager != null)
            locationManager.removeUpdates(this);

    }

}