package com.amd.carmain.view;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.provider.Settings;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.amd.carmain.api.ModuleAPI;
import com.amd.carmain.R;
import com.amd.carmain.api.ModuleAPIUtil;
import com.amd.carmain.data.ChecklistDB;
import com.amd.carmain.data.ExpireDB;
import com.amd.carmain.data.model.CarKilometer;
import com.amd.carmain.data.model.Checklist;
import com.amd.carmain.data.model.Expire;
import com.amd.carmain.service.NSDLookupHelper;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.amd.carmain.data.DateFormatterUtil.dateFormatter;
import static com.amd.carmain.data.model.Expire.EXPIRE_DATE;
import static com.amd.carmain.data.model.Expire.EXPIRE_KILOMETER;

public class ChecklistDetailActivity extends AppCompatActivity {

    public final static String CHECKLIST_ID = "checklist_id";

    private boolean expireMode;

    private boolean renewMode;
    private int checklistId;
    private int expireId;

    private TextInputLayout tilTitle;
    private AutoCompleteTextView atvTitle;
    private EditText evNote;
    private TextInputLayout tilChangeDate;
    private EditText evChangeDate;
    private TextInputLayout tilChangeKilometer;
    private EditText evChangeKilometer;
    private TextInputLayout tilExpireDate;
    private EditText evExpireDate;
    private TextInputLayout tilExpireKilometer;
    private EditText evExpireKilometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checklist_detail);

        initViews();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            renewMode = true;
            loadChecklist(extras.getInt(CHECKLIST_ID));
        } else {
            renewMode = false;
            setExpireMode(EXPIRE_DATE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.checklist_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_submit:
                submit();
            break;
        }
        return true;
    }

    private void initViews() {

        Toolbar toolbar = findViewById(R.id.topAppBar_checklist);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        tilTitle = findViewById(R.id.textInputLayout_detail_title);
        atvTitle = findViewById(R.id.autoCompleteText_detail_title);
        evNote = findViewById(R.id.editText_detail_note);
        tilChangeDate = findViewById(R.id.textInputLayout_detail_changeDate);
        evChangeDate = findViewById(R.id.editText_detail_changeDate);
        tilChangeKilometer = findViewById(R.id.textInputLayout_detail_changeKilometer);
        evChangeKilometer = findViewById(R.id.editText_detail_changeKilometer);
        tilExpireDate = findViewById(R.id.textInputLayout_detail_expireDate);
        evExpireDate = findViewById(R.id.editText_detail_expireDate);
        tilExpireKilometer = findViewById(R.id.textInputLayout_detail_expireKilometer);
        evExpireKilometer = findViewById(R.id.editText_detail_expireKilometer);

        Resources res = getResources();
        String[] type = res.getStringArray(R.array.maintenance_array);
        ArrayAdapter<?> adapter = new ArrayAdapter<>(this, R.layout.cheklist_title_item, type);
        atvTitle.setAdapter(adapter);

        tilChangeDate.setStartIconCheckable(true);
        tilChangeDate.setStartIconOnClickListener(v -> {
            Date currentDate = Calendar.getInstance().getTime();
            evChangeDate.setText(dateFormatter(currentDate));
        });
        evChangeDate.setInputType(InputType.TYPE_NULL);
        evChangeDate.setOnClickListener(v -> {
            datePicker(evChangeDate);
        });
        evChangeDate.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) datePicker(evChangeDate);
        });

        tilChangeKilometer.setStartIconCheckable(true);
        tilChangeKilometer.setStartIconOnClickListener(v -> {
            getCurrentKilometerListener();
        });

        evExpireDate.setInputType(InputType.TYPE_NULL);
        evExpireDate.setOnClickListener(v -> {
            datePicker(evExpireDate);
        });
        evExpireDate.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) datePicker(evExpireDate);
        });

        tilExpireDate.setStartIconCheckable(true);
        tilExpireDate.setStartIconOnClickListener(v ->
                setExpireMode(EXPIRE_DATE)
        );

        tilExpireKilometer.setStartIconCheckable(true);
        tilExpireKilometer.setStartIconOnClickListener(v ->
                setExpireMode(EXPIRE_KILOMETER)
        );
    }

    private void loadChecklist(int id) {
        ChecklistDB db = new ChecklistDB(this);
        Checklist checklist = db.findById(id);

        checklistId = checklist.getId();
        expireId = checklist.getExpire().getId();

        atvTitle.setText(checklist.getTitle());
        evNote.setText(checklist.getNote());
        evChangeKilometer.setText(String.valueOf(checklist.getChangeKilometer()));
        evChangeDate.setText(dateFormatter(checklist.getChangeDate()));

        if (checklist.getExpire().expireMode() == EXPIRE_DATE)
            setExpireMode(EXPIRE_DATE);
        else
            setExpireMode(EXPIRE_KILOMETER);

        evExpireKilometer.setText(String.valueOf(checklist.getExpire().getExpireKilometer()));
        if (checklist.getExpire().getExpireDate() != null)
            evExpireDate.setText(dateFormatter(checklist.getExpire().getExpireDate()));
    }

    void setExpireMode(boolean b) {
        if (b == EXPIRE_KILOMETER) {
            tilExpireKilometer.setStartIconTintList(ColorStateList.valueOf(getResources().getColor(R.color.green_600)));
            tilExpireDate.setStartIconTintList(ColorStateList.valueOf(getResources().getColor(R.color.red_500)));
            expireMode = EXPIRE_KILOMETER;
        } else if (b == EXPIRE_DATE) {
            tilExpireDate.setStartIconTintList(ColorStateList.valueOf(getResources().getColor(R.color.green_600)));
            tilExpireKilometer.setStartIconTintList(ColorStateList.valueOf(getResources().getColor(R.color.red_500)));
            expireMode = EXPIRE_DATE;
        }
    }

    private void submit() {
        boolean flag = true;

        tilTitle.setErrorEnabled(false);
        tilChangeDate.setErrorEnabled(false);
        tilChangeKilometer.setErrorEnabled(false);
        tilExpireDate.setErrorEnabled(false);
        tilExpireKilometer.setErrorEnabled(false);

        if (TextUtils.isEmpty(atvTitle.getText())) {
            tilTitle.setError("Required");
            flag = false;
        }
        if (TextUtils.isEmpty(evChangeDate.getText())) {
            tilChangeDate.setError("Required");
            flag = false;
        }
        if (TextUtils.isEmpty(evChangeKilometer.getText())) {
            tilChangeKilometer.setError("Required");
            flag = false;
        }
        if ((expireMode == EXPIRE_DATE) && TextUtils.isEmpty(evExpireDate.getText())) {
            tilExpireDate.setError("Required");
            flag = false;
        }
        if ((expireMode == EXPIRE_KILOMETER) && TextUtils.isEmpty(evExpireKilometer.getText())) {
            tilExpireKilometer.setError("Required");
            flag = false;
        }

        if (flag) {
            ChecklistDB db = new ChecklistDB(this);

            Checklist checklist = new Checklist();
            checklist.setTitle(atvTitle.getText().toString());
            checklist.setNote(evNote.getText().toString());
            checklist.setChangeKilometer(Long.parseLong(evChangeKilometer.getText().toString()));
            checklist.setChangeDate(dateFormatter(evChangeDate.getText().toString()));

            Expire expire = new Expire();
            expire.setExpireMode(expireMode);
            if (!TextUtils.isEmpty(evExpireKilometer.getText()))
                expire.setExpireKilometer(Long.parseLong(evExpireKilometer.getText().toString()));
            if (!TextUtils.isEmpty(evExpireDate.getText()))
                expire.setExpireDate(dateFormatter(evExpireDate.getText().toString()));
            if (expireMode == EXPIRE_DATE && dateFormatter(evExpireDate.getText().toString()).before(Calendar.getInstance().getTime())) {
                expire.setExpired(true);
            } else {
                expire.setExpired(false);
            }
            expire.setNotificationOn(true);

            checklist.setExpire(expire);

            if (renewMode) {
                ExpireDB expireDB = new ExpireDB(this);
                checklist.setId(checklistId);
                checklist.getExpire().setId(expireId);
                expireDB.update(checklist.getExpire());
                db.update(checklist);
            } else
                db.create(checklist);

            onBackPressed();
        }
    }

    private void getCurrentKilometerListener() {
        NSDLookupHelper nsdLookup = NSDLookupHelper.getInstance(this);

        if (!nsdLookup.isServiceOn())  // start nsdLookup service if service is off
            nsdLookup.registerService();

        ModuleAPI api = ModuleAPIUtil.newInstance(nsdLookup);

        Call<CarKilometer> call = api.getKilometer(nsdLookup.getModuleURI());

        call.enqueue(new Callback<CarKilometer>() {
            @Override
            public void onResponse(Call<CarKilometer> call, Response<CarKilometer> response) {
                if (response.code() == 200) {
                    String kilometer = String.valueOf(response.body().getKilometer());
                    evChangeKilometer.setText(kilometer);
                }
            }

            @Override
            public void onFailure(Call<CarKilometer> call, Throwable t) {
                Snackbar.make(findViewById(R.id.relativeLayout_checklistDetail_mainLayout),
                        "Please make sure wifi module connected", Snackbar.LENGTH_LONG)
                        .setAction("Wifi setting", view1 ->
                                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)))
                        .show();
            }
        });
    }

    private void datePicker(EditText view) {
        MaterialDatePicker.Builder<Long> materialDateBuilder = MaterialDatePicker.Builder.datePicker();
        materialDateBuilder.setTitleText("SELECT A DATE");
        final MaterialDatePicker<Long> datePicker = materialDateBuilder.build();

        datePicker.show(getSupportFragmentManager(), "MATERIAL_DATE_PICKER");


        datePicker.addOnPositiveButtonClickListener(selectedDate ->
                view.setText(datePicker.getHeaderText()));
    }
}