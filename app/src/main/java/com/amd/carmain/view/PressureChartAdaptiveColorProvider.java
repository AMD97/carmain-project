package com.amd.carmain.view;

import androidx.core.graphics.ColorUtils;

import com.ramijemli.percentagechartview.callback.AdaptiveColorProvider;

public class PressureChartAdaptiveColorProvider implements AdaptiveColorProvider {

    private final int normalLevelColor;
    private final int warningLevelColor;
    private final int dangerRangeColor;
    private final int backgroundColor;
    private final int backgroundBarColor;
    private final int textColor;

    public PressureChartAdaptiveColorProvider(int normalLevelColor, int warningLevelColor, int dangerRangeColor, int backgroundColor, int backgroundBarColor, int textColor) {
        this.normalLevelColor = normalLevelColor;
        this.warningLevelColor = warningLevelColor;
        this.dangerRangeColor = dangerRangeColor;
        this.backgroundColor = backgroundColor;
        this.backgroundBarColor = backgroundBarColor;
        this.textColor = textColor;
    }

    @Override
    public int provideProgressColor(float progress) {
        if (progress < 25)
            return dangerRangeColor;
        else if (progress < 30)
            return warningLevelColor;
        else if (progress < 36)
            return normalLevelColor;
        else if (progress < 40)
            return warningLevelColor;
        else
            return dangerRangeColor;
    }

    @Override
    public int provideBackgroundColor(float progress) {
        return ColorUtils.blendARGB(provideProgressColor(progress),
                backgroundColor,
                .8f);
    }

    @Override
    public int provideBackgroundBarColor(float progress) {
        return backgroundBarColor;
    }

    @Override
    public int provideTextColor(float progress) {
        return textColor;
    }

}
