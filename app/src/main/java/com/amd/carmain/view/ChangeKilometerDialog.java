package com.amd.carmain.view;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.amd.carmain.R;
import com.amd.carmain.api.ModuleAPI;
import com.amd.carmain.api.ModuleAPIUtil;
import com.amd.carmain.service.NSDLookupHelper;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeKilometerDialog extends DialogFragment {

    private TextInputLayout tilKilometer;
    private TextInputEditText etKilometer;

    private Button btnChange;
    private Button btnCancel;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_change_kilometer, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDialog().setCancelable(false);
        getDialog().setTitle("Change Fake Kilometer");

        tilKilometer = view.findViewById(R.id.textInputLayout_changeKilometer);
        etKilometer = view.findViewById(R.id.editText_changeKilometer);
        btnChange = view.findViewById(R.id.button_numberPicker_change);
        btnCancel = view.findViewById(R.id.button_numberPicker_cancel);

        btnChange.setOnClickListener(v -> changeListener());
        btnCancel.setOnClickListener(v -> cancelListener());

        //Show soft keyboard automatically
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    private void changeListener() {

        tilKilometer.setErrorEnabled(false);

        String value = etKilometer.getText().toString();
        if (!TextUtils.isEmpty(value) && TextUtils.isDigitsOnly(value)) {

            ProgressDialog progressdialog = new ProgressDialog(getContext());
            progressdialog.setMessage(getString(R.string.please_wait));
            progressdialog.setCancelable(false);
            progressdialog.show();

            NSDLookupHelper nsdLookup = NSDLookupHelper.getInstance(getActivity());
            if (!nsdLookup.isServiceOn())
                nsdLookup.registerService();

            ModuleAPI api = ModuleAPIUtil.newInstance(nsdLookup);
            Call<Void> call = api.setKilometer(nsdLookup.getModuleURI(), value);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    progressdialog.dismiss();
                    Objects.requireNonNull(ChangeKilometerDialog.this.getDialog()).cancel();
                    String message;
                    if (response.code() == 200)
                        message = getString(R.string.change_fake_kilometer_successfully);
                    else
                        message = getString(R.string.change_fake_kilometer_unsuccessfully);

                    assert getParentFragment() != null;
                    Snackbar.make(getParentFragment().getView().findViewById(R.id.coordinatorLayout_setting),
                            message, Snackbar.LENGTH_LONG)
                            .show();
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    progressdialog.dismiss();
                    Objects.requireNonNull(ChangeKilometerDialog.this.getDialog()).cancel();
                    Snackbar.make(getParentFragment().getView().findViewById(R.id.coordinatorLayout_setting),
                            R.string.make_sure_wifi_connected, Snackbar.LENGTH_LONG)
                            .setAction(R.string.wifi_setting, view1 ->
                                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)))
                            .show();
                }
            });
        } else {
            tilKilometer.setError(getString(R.string.required));
        }

    }

    private void cancelListener() {
            Objects.requireNonNull(ChangeKilometerDialog.this.getDialog()).cancel();
    }
}
