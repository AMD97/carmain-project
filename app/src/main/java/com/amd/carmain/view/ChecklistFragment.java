package com.amd.carmain.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amd.carmain.view.adapter.ChecklistItemRecyclerViewAdapter;
import com.amd.carmain.R;
import com.amd.carmain.data.ChecklistDB;
import com.amd.carmain.data.model.Checklist;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class ChecklistFragment extends Fragment {

    private Toolbar toolbar;
    private ChecklistDB checklistDB;
    private RecyclerView rvReminder;
    private FloatingActionButton fabAdd;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_checklist, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    private void initViews(View view) {

        toolbar = view.findViewById(R.id.topAppBar_checklist);
        toolbar.setTitle("");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        rvReminder = view.findViewById(R.id.recycler_checklist);
        fabAdd = view.findViewById(R.id.floatingActionButton);

        fabAdd.setOnClickListener(v -> {
            Intent myIntent = new Intent(getContext(), ChecklistDetailActivity.class);
            startActivity(myIntent);
        });
    }

    private void loadData() {

        if (checklistDB == null)
            checklistDB = new ChecklistDB(getContext());

        List<Checklist> list = checklistDB.findAll();

        ChecklistItemRecyclerViewAdapter adapter = new ChecklistItemRecyclerViewAdapter(list);

        rvReminder.setAdapter(adapter);
        rvReminder.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.checklist_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {


        ChecklistItemRecyclerViewAdapter adapter = (ChecklistItemRecyclerViewAdapter) rvReminder.getAdapter();

        switch (item.getItemId()) {
            case R.id.action_title_name:
                adapter.sort(ChecklistItemRecyclerViewAdapter.Sort.TITLE);
                item.setChecked(true);
                return true;
            case R.id.action_sort_title_desc:
                adapter.sort(ChecklistItemRecyclerViewAdapter.Sort.TITLE_DESC);
                item.setChecked(true);
                return true;
            case R.id.action_sort_date:
                adapter.sort(ChecklistItemRecyclerViewAdapter.Sort.DATE);
                item.setChecked(true);
                return true;
            case R.id.action_sort_date_desc:
                adapter.sort(ChecklistItemRecyclerViewAdapter.Sort.DATE_DESC);
                item.setChecked(true);
                return true;
            case R.id.action_sort_expired:
                adapter.sort(ChecklistItemRecyclerViewAdapter.Sort.EXPIRED);
                item.setChecked(true);
                return true;
            case R.id.action_sort_expired_desc:
                adapter.sort(ChecklistItemRecyclerViewAdapter.Sort.EXPIRED_DESC);
                item.setChecked(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }
}

