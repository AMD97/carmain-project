package com.amd.carmain.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarKilometer {

    @SerializedName("kilometer")
    @Expose
    private long kilometer;

    public CarKilometer() {
    }

    public CarKilometer(long kilometer) {
        this.kilometer = kilometer;
    }

    public long getKilometer() {
        return kilometer;
    }
}
