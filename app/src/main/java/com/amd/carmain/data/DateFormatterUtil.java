package com.amd.carmain.data;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateFormatterUtil {

    private static final String TAG = "DateFormatterUtil";

    public static Date dateFormatter(String strDate) {
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());
        try {
            return dateFormat.parse(strDate);
        } catch (ParseException e) {
            Log.e(TAG, e.getMessage());
            return Calendar.getInstance().getTime();
        }
    }

    public static String dateFormatter(Date date) {
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());
        return dateFormat.format(date);
    }


}
