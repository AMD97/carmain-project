package com.amd.carmain.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "checklist")
public class Checklist {

    public static final String ID_FIELD_NAME = "id";
    public static final String TITLE_FIELD_NAME = "title";
    public static final String NOTE_FIELD_NAME = "note";
    public static final String CHANGE_KILOMETER_FIELD_NAME = "change_kilometer";
    public static final String CHANGE_DATE_FIELD_NAME = "change_date";
    public static final String EXPIRE_FIELD_NAME = "expire";

    @DatabaseField(generatedId = true, columnName = ID_FIELD_NAME)
    private int id;

    @DatabaseField(canBeNull = false, columnName = TITLE_FIELD_NAME)
    private String title;

    @DatabaseField(canBeNull = true, columnName = NOTE_FIELD_NAME)
    private String note;

    @DatabaseField(canBeNull = false, columnName = CHANGE_KILOMETER_FIELD_NAME)
    private long changeKilometer;

    @DatabaseField(canBeNull = false, columnName = CHANGE_DATE_FIELD_NAME)
    private Date changeDate;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true, columnName = EXPIRE_FIELD_NAME)
    private Expire expire;

    public Checklist() {
    }

    public Checklist(String title, String note, long changeKilometer, Date changeDate, Expire expire) {
        this.title = title;
        this.note = note;
        this.changeKilometer = changeKilometer;
        this.changeDate = changeDate;
        this.expire = expire;
    }

    public Checklist(int id, String title, String note, long changeKilometer, Date changeDate, Expire expire) {
        this.id = id;
        this.title = title;
        this.note = note;
        this.changeKilometer = changeKilometer;
        this.changeDate = changeDate;
        this.expire = expire;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public long getChangeKilometer() {
        return changeKilometer;
    }

    public void setChangeKilometer(long changeKilometer) {
        this.changeKilometer = changeKilometer;
    }

    public Date getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

    public Expire getExpire() {
        return expire;
    }

    public void setExpire(Expire expire) {
        this.expire = expire;
    }
}
