package com.amd.carmain.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarDiagnostic {

    @SerializedName("oil")
    @Expose
    private boolean oil;

    @SerializedName("water")
    @Expose
    private boolean water;

    @SerializedName("brakePad")
    @Expose
    private boolean brakePad;

    @SerializedName("fuel")
    @Expose
    private float fuel;

    @SerializedName("tirePressureFR")
    @Expose
    private float tirePressureFR;

    @SerializedName("tirePressureFL")
    @Expose
    private float tirePressureFL;

    @SerializedName("tirePressureBR")
    @Expose
    private float tirePressureBR;

    @SerializedName("tirePressureBL")
    @Expose
    private float tirePressureBL;

    public CarDiagnostic() {
    }

    public CarDiagnostic(boolean oil, boolean water, boolean brakePad, float fuel, float tirePressureFR, float tirePressureFL, float tirePressureBR, float tirePressureBL) {
        this.oil = oil;
        this.water = water;
        this.brakePad = brakePad;
        this.fuel = fuel;
        this.tirePressureFR = tirePressureFR;
        this.tirePressureFL = tirePressureFL;
        this.tirePressureBR = tirePressureBR;
        this.tirePressureBL = tirePressureBL;
    }

    public boolean getOil() {
        return oil;
    }

    public boolean getWater() {
        return water;
    }

    public boolean getBrakePad() {
        return brakePad;
    }

    public float getFuel() {
        return fuel;
    }

    public float getTirePressureFR() {
        return tirePressureFR;
    }

    public float getTirePressureFL() {
        return tirePressureFL;
    }

    public float getTirePressureBR() {
        return tirePressureBR;
    }

    public float getTirePressureBL() {
        return tirePressureBL;
    }

}