package com.amd.carmain.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "checklist_expire")
public class Expire {

    public static final String ID_FIELD_NAME = "id";
    public static final String EXPIRE_MODE_FIELD_NAME = "expire_mode";
    public static final String EXPIRE_KILOMETER_FIELD_NAME = "expire_kilometer";
    public static final String EXPIRE_DATE_FIELD_NAME = "expire_date";
    public static final String EXPIRED_FIELD_NAME = "expired";
    public static final String NOTIFICATION_FIELD_NAME = "notification";

    public static final boolean EXPIRE_DATE = true;
    public static final boolean EXPIRE_KILOMETER = false;

    @DatabaseField(generatedId = true, columnName = ID_FIELD_NAME)
    private int id;

    @DatabaseField(canBeNull = false, columnName = EXPIRE_MODE_FIELD_NAME)
    private boolean expireMode;

    @DatabaseField(canBeNull = true, columnName = EXPIRE_KILOMETER_FIELD_NAME)
    private long expireKilometer;

    @DatabaseField(canBeNull = true, columnName = EXPIRE_DATE_FIELD_NAME)
    private Date expireDate;

    @DatabaseField(canBeNull = false, columnName = EXPIRED_FIELD_NAME)
    private boolean expired;

    @DatabaseField(canBeNull = false, columnName = NOTIFICATION_FIELD_NAME)
    private boolean notificationOn;

    public Expire() {
    }

    public Expire(boolean expireMode, long expireKilometer, Date expireDate, boolean expired, boolean notificationOn) {
        this.expireMode = expireMode;
        this.expireKilometer = expireKilometer;
        this.expireDate = expireDate;
        this.expired = expired;
        this.notificationOn = notificationOn;
    }

    public Expire(int id, boolean expireMode, long expireKilometer, Date expireDate, boolean expired, boolean notificationOn) {
        this.id = id;
        this.expireMode = expireMode;
        this.expireKilometer = expireKilometer;
        this.expireDate = expireDate;
        this.expired = expired;
        this.notificationOn = notificationOn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean expireMode() {
        return expireMode;
    }

    public void setExpireMode(boolean expireMode) {
        this.expireMode = expireMode;
    }

    public long getExpireKilometer() {
        return expireKilometer;
    }

    public void setExpireKilometer(long expireKilometer) {
        this.expireKilometer = expireKilometer;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public boolean isNotificationOn() {
        return notificationOn;
    }

    public void setNotificationOn(boolean notificationOn) {
        this.notificationOn = notificationOn;
    }
}
