package com.amd.carmain.data;

import android.content.Context;
import android.util.Log;


import com.amd.carmain.data.model.Expire;

import java.sql.SQLException;
import java.util.List;

import static com.amd.carmain.data.model.Expire.EXPIRED_FIELD_NAME;


public class ExpireDB implements CRUD<Expire>{

    private final String TAG = "ExpireDB";

    private final DatabaseHelper dbContext;

    public ExpireDB(Context context) {
        this.dbContext = new DatabaseHelper(context);
    }

    @Override
    public int create(Expire object) {
        int index = -1;
        try {
            index = dbContext.expireDao().create(object);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return index;
    }

    @Override
    public int update(Expire object) {
        int index = -1;
        try {
            dbContext.expireDao().update(object);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return index;
    }

    @Override
    public int delete(Expire object) {
        int index = -1;
        try {
            dbContext.expireDao().delete(object);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return index;
    }

    @Override
    public Expire findById(int id) {
        Expire item = null;
        try {
            item = dbContext.expireDao().queryForId(id);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return item;
    }

    @Override
    public List<Expire> findAll() {
        List<Expire> items = null;
        try {
            items = dbContext.expireDao().queryForAll();
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return items;
    }

    public List<Expire> findUnexpirerd() {
        List<Expire> items = null;
        try {
            items = dbContext.expireDao()
                    .queryBuilder()
                    .where()
                    .eq(EXPIRED_FIELD_NAME, false)
                    .query();
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return items;
    }

}
