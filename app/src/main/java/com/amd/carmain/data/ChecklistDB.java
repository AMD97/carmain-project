package com.amd.carmain.data;

import android.content.Context;
import android.util.Log;

import com.amd.carmain.data.model.Checklist;
import com.amd.carmain.data.model.Expire;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.amd.carmain.data.model.Checklist.EXPIRE_FIELD_NAME;
import static com.amd.carmain.data.model.Expire.EXPIRED_FIELD_NAME;
import static com.amd.carmain.data.model.Expire.ID_FIELD_NAME;

public class ChecklistDB implements CRUD<Checklist> {
    private final String TAG = "ChecklistDB";
    private final DatabaseHelper dbContext;

    public ChecklistDB(Context context) {
        this.dbContext = new DatabaseHelper(context);
    }

    @Override
    public int create(Checklist object) {
        int index = -1;
        try {
            index = dbContext.checklistDao().create(object);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return index;
    }

    @Override
    public int update(Checklist object) {
        int index = -1;
        try {
            dbContext.checklistDao().update(object);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return index;
    }

    public Dao.CreateOrUpdateStatus createOrUpdate(Checklist object) {
        Dao.CreateOrUpdateStatus result = null;
        try {
            result = dbContext.checklistDao().createOrUpdate(object);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return result;
    }
    @Override
    public int delete(Checklist object) {
        int index = -1;
        try {
            dbContext.checklistDao().delete(object);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return index;
    }

    @Override
    public Checklist findById(int id) {
        Checklist checklist = null;
        try {
            checklist = dbContext.checklistDao().queryForId(id);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return checklist;
    }

    @Override
    public List<Checklist> findAll() {
        List<Checklist> items = null;
        try {
            items = dbContext.checklistDao().queryForAll();
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return items;
    }

    public List<Checklist> findUnexpired() {
        List<Checklist> items;
        try {
            QueryBuilder<Expire, Integer>  expiredQB =
                    dbContext.expireDao().queryBuilder();
            expiredQB.selectColumns(ID_FIELD_NAME)
                    .where()
                    .eq(EXPIRED_FIELD_NAME, false);

            QueryBuilder<Checklist, Integer> checklistQb =
                    dbContext.checklistDao().queryBuilder();
            checklistQb.where()
                    .in(EXPIRE_FIELD_NAME, expiredQB);

            items = checklistQb.query();
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
            items = new ArrayList<Checklist>();
        }
        return items;
    }
}
