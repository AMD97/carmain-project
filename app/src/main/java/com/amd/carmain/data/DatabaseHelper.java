package com.amd.carmain.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.amd.carmain.data.model.Checklist;
import com.amd.carmain.data.model.Expire;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private final String TAG =  "DatabaseHelper";

    private static final String DATABASE_NAME = "car.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<Checklist, Integer> checklistDao = null;
    private Dao<Expire, Integer> expireDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, Checklist.class);
            TableUtils.createTable(connectionSource, Expire.class);

        } catch (SQLException e) {
            Log.e(TAG, "Can't create database", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, Checklist.class, true);
            TableUtils.dropTable(connectionSource, Expire.class, true);
            // after we drop the old databases, we create the new ones
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            Log.e(TAG, "Can't drop databases", e);
        }
    }

    public Dao<Checklist, Integer> checklistDao() {

        if (checklistDao == null) {
            try {
                checklistDao = getDao(Checklist.class);
            } catch (java.sql.SQLException e) {
                Log.d(TAG, e.getMessage());
            }
        }
        return checklistDao;
    }

    public Dao<Expire, Integer> expireDao() {

        if (expireDao == null) {
            try {
                expireDao = getDao(Expire.class);
            } catch (java.sql.SQLException e) {
                Log.d(TAG, e.getMessage());
            }
        }
        return expireDao;
    }

    @Override
    public void close() {
        super.close();
        checklistDao = null;
        expireDao = null;
    }
}
