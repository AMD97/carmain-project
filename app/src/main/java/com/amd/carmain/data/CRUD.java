package com.amd.carmain.data;

import java.util.List;

public interface CRUD<T> {

    public int create(T object);

    public int update(T object);

    public int delete(T object);

    public T findById(int id);

    public List<T> findAll();

}
