package com.amd.carmain.api;


import com.amd.carmain.data.model.CarDiagnostic;
import com.amd.carmain.data.model.CarKilometer;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ModuleAPI {

    @GET("{fullUrl}")
    Observable<CarDiagnostic> getDiagnostic(@Path(value = "fullUrl", encoded = true) String fullUrl);

    @GET("{fullUrl}" + "/kilometer")
    Call<CarKilometer> getKilometer(@Path(value = "fullUrl", encoded = true) String fullUrl);

    @GET("{fullUrl}" + "/kilometer")
    Call<Void> setKilometer(@Path(value = "fullUrl", encoded = true) String fullUrl,
                            @Query("update") String update);

    @GET("{fullUrl}" + "/wificredential")
    Call<Void> setWifiCredential(@Path(value = "fullUrl", encoded = true) String fullUrl,
                                     @Query(value = "ssid", encoded = true) String ssid,
                                     @Query(value = "password", encoded = true) String password);
}
