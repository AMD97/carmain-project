package com.amd.carmain.api;

import com.amd.carmain.service.NSDLookupHelper;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ModuleAPIUtil {
    public static ModuleAPI newInstance(NSDLookupHelper nsdLookupHelper) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(nsdLookupHelper.getModuleURI())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return retrofit.create(ModuleAPI.class);
    }
}
