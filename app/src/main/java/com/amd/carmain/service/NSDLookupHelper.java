package com.amd.carmain.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.util.Log;

import java.net.InetAddress;

public class NSDLookupHelper {

    private static NSDLookupHelper instance;

    private final String SERVICE_TYPE = "_http._tcp.";
    private final String serviceName = "carmain";

    private Context context;
    private boolean serviceOn;
    private NsdManager nsdManager;
    private NsdManager.DiscoveryListener discoveryListener;
    private NsdManager.ResolveListener resolveListener;

    private String ModuleURI;

    private final String URI_KEY = "uriKey";
    private final SharedPreferences sharedPref;

    private final String TAG = "NSDLookupHelper";

    private NSDLookupHelper(Context context) {
        this.context = context;
        sharedPref = context.getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE);
    }

    public static NSDLookupHelper getInstance(Context context) {
        if (instance == null) {
            synchronized (NSDLookupHelper.class) {
                if (instance == null) {
                    instance = new NSDLookupHelper(context);
                }
            }
        }
        return instance;
    }

    public void registerService() {
        Log.d(TAG, "Service registered");

        serviceOn(true);

        ModuleURI = sharedPref.getString(URI_KEY, "192.168.1.1");

        nsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);

        initializeDiscoveryListener();
        initializeResolveListener();

        nsdManager.discoverServices(
                SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, discoveryListener);
    }

    private void initializeDiscoveryListener() {

        // Instantiate a new DiscoveryListener
        discoveryListener = new NsdManager.DiscoveryListener() {

            // Called as soon as service discovery begins.
            @Override
            public void onDiscoveryStarted(String regType) {
                Log.d(TAG, "Service discovery started");
            }

            @Override
            public void onServiceFound(NsdServiceInfo service) {
                // A service was found! Do something with it.
                Log.d(TAG, "Service discovery success" + service);
                if (service.getServiceName().contains(serviceName)) {
                    nsdManager.resolveService(service, resolveListener);
                }
            }

            @Override
            public void onServiceLost(NsdServiceInfo service) {
                // When the network service is no longer available.
                // Internal bookkeeping code goes here.
                Log.d(TAG, "service lost: " + service);
                stopService();
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.d(TAG, "Discovery stopped: " + serviceType);
            }

            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                Log.d(TAG, "Discovery failed: Error code:" + errorCode);
                nsdManager.stopServiceDiscovery(this);
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                Log.d(TAG, "Discovery failed: Error code:" + errorCode);
                nsdManager.stopServiceDiscovery(this);
            }
        };
    }

    private void initializeResolveListener() {
        resolveListener = new NsdManager.ResolveListener() {

            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                Log.d(TAG, "Resolve failed: " + errorCode);
                stopService();
            }

            @Override
            public void onServiceResolved(NsdServiceInfo serviceInfo) {
                Log.d(TAG, "Resolve Succeeded. " + serviceInfo);

                InetAddress host = serviceInfo.getHost();
                String address = host.getHostAddress();
                if (!ModuleURI.equals(address)) {
                    ModuleURI = address;
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(URI_KEY, address);
                    editor.apply();
                    Log.e(TAG, "URI Updated. " + serviceInfo);
                    stopService();
                }
            }
        };
    }

    public String getModuleURI() {
        return "http://" + ModuleURI;
    }

    public void stopService() {
        Log.d(TAG, "service stopped");
        serviceOn(false);
        nsdManager.stopServiceDiscovery(discoveryListener);
    }

    public boolean isServiceOn() {
        return serviceOn;
    }

    private void serviceOn(boolean serviceOn) {
        Log.d(TAG, serviceOn ? "serviceOn": "serviceOff");
        this.serviceOn = serviceOn;
    }
}
