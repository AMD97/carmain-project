package com.amd.carmain.service;

import android.app.NotificationManager;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.os.StrictMode;
import android.util.Log;

import com.amd.carmain.api.ModuleAPI;
import com.amd.carmain.api.ModuleAPIUtil;
import com.amd.carmain.data.ChecklistDB;
import com.amd.carmain.data.ExpireDB;
import com.amd.carmain.data.model.CarKilometer;
import com.amd.carmain.data.model.Checklist;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.amd.carmain.data.model.Expire.EXPIRE_KILOMETER;
import static com.amd.carmain.service.NotificationBuilder.createNotification;

public class ChecklistKilometerJobService extends JobService {

    private final String TAG = "ChecklistKMJobService";

    ChecklistDB checklistDB;
    ExpireDB expireDB;

    @Override
    public boolean onStartJob(JobParameters params) {
        expireChecking();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

    private void expireChecking() {
        Log.d(TAG, "expireChecking");

        NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        checklistDB = new ChecklistDB(this);
        expireDB = new ExpireDB(this);

        List<Checklist> checklists = checklistDB.findUnexpired();
        long currentKilometer = getCurrentKilometer();

        for (Checklist item : checklists) {
            if (currentKilometer != -1                                           //  (if current kilometer is available  (-1 meaning unavailable)
                    && item.getExpire().expireMode() == EXPIRE_KILOMETER          //  and expire by kilometer
                    && item.getExpire().getExpireKilometer() <= currentKilometer)    //  and expired kilometer value)
            {
                //  send notification
                if (item.getExpire().isNotificationOn())
                    mNotifyManager.notify(item.getId(), createNotification(this, item.getTitle()));

                //  update value
                item.getExpire().setExpired(true);
                item.getExpire().setNotificationOn(false);
                expireDB.update(item.getExpire());
            }
        }

    }

    private long getCurrentKilometer() {

        Log.d(TAG, "getCurrentKilometer");

        //policy needed to create a network connection in the service
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        NSDLookupHelper nsdLookup = NSDLookupHelper.getInstance(this);
        if (!nsdLookup.isServiceOn()) { // start nsdLookup service if service is off
            nsdLookup.registerService();
        }

        ModuleAPI api = ModuleAPIUtil.newInstance(nsdLookup);
        Call<CarKilometer> call = api.getKilometer(nsdLookup.getModuleURI());
        long km = -1;
        try {
            Response<CarKilometer> response = call.execute();
            if (response.code() == 200) {
                CarKilometer kilometer = response.body();
                km = kilometer.getKilometer();
            }
        } catch (Exception e) {
            Log.e(TAG, "getCurrentKilometer: " + e.toString());
            return km; // return -1 if unavailable
        }
        return km;
    }

}
