package com.amd.carmain.service;

import android.app.NotificationManager;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.util.Log;

import com.amd.carmain.data.ChecklistDB;
import com.amd.carmain.data.ExpireDB;
import com.amd.carmain.data.model.Checklist;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.amd.carmain.data.model.Expire.EXPIRE_DATE;
import static com.amd.carmain.service.NotificationBuilder.createNotification;

public class ChecklistDateJobService extends JobService {

    private final String TAG = "ChecklistDateService";

    ChecklistDB checklistDB;
    ExpireDB expireDB;

    @Override
    public boolean onStartJob(JobParameters params) {
        expireChecking();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

    private void expireChecking() {
        Log.d(TAG, "expireChecking");

        NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        checklistDB = new ChecklistDB(this);
        expireDB = new ExpireDB(this);

        List<Checklist> checklists = checklistDB.findUnexpired();
        Date currentDate = Calendar.getInstance().getTime();

        for (Checklist item : checklists) {
            if (item.getExpire().expireMode() == EXPIRE_DATE &&                 //  (if expire by date
                    item.getExpire().getExpireDate().before(currentDate))    //  and expired) OR
            {
                //  send notification
                if (item.getExpire().isNotificationOn())
                    mNotifyManager.notify(item.getId(), createNotification(this, item.getTitle()));

                //  update value
                item.getExpire().setExpired(true);
                item.getExpire().setNotificationOn(false);
                expireDB.update(item.getExpire());
            }
        }
    }


}
